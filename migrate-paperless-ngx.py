#!/usr/bin/python3

# migrate-paperless-ngx
# Copyright (C) 2023 Henrik Busch
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import urllib3
import os
import argparse
import re
from pathlib import Path

name_regex = '[^a-zäöüßA-ZÄÖÜ0-9\s\.\(\)\-_]'
date_regex = '[^a-zäöüßA-ZÄÖÜ0-9\s]'

def get_tag_by_name(base_url, token, name):
    response = requests.get(base_url + "/api/tags/?name__iexact=" + name, headers={"Authorization":"Token " + token})
    for result in response.json()["results"]:
        if result["name"] == name:
            return result["id"]
    return -1

def create_tag(base_url, token, name):
    tag_json = {"name": name, "matching_algorithm": 6}
    response = requests.post(base_url + "/api/tags/", headers={"Authorization":"Token " + token}, json=tag_json)
    return response.json()["id"]

def get_correspondent_by_name(base_url, token, name):
    response = requests.get(base_url + "/api/correspondents/?name__iexact=" + name, headers={"Authorization":"Token " + token})
    for result in response.json()["results"]:
        if result["name"] == name:
            return result["id"]
    return -1

def create_correspondent(base_url, token, name):
    correspondent_json = {"name": name, "matching_algorithm": 6}
    response = requests.post(base_url + "/api/correspondents/", headers={"Authorization":"Token " + token}, json=correspondent_json)
    return response.json()["id"]

def get_name_from_filename(filename):
    filename_clean = re.sub(name_regex, '', Path(filename).stem)
    words = os.path.splitext(filename_clean)[0].split()
    
    if not words:
        raise AttributeError("Error: Could not determine name from filename " + filename)

    if len(words) <= 1:
        return filename_clean

    if words[0].isnumeric():
        if len(words[0]) % 2 != 0:
            return filename_clean
        else:
            return " ".join(words[1:])
    else:
        return filename_clean

def get_date_from_filename(filename):
    filename_clean = re.sub(date_regex, '', Path(filename).stem)
    words = os.path.splitext(filename_clean)[0].split()

    if not words:
        raise AttributeError("Error: Could not determine date from filename " + filename)
    
    if words[0].isnumeric():
        if len(words[0]) % 2 != 0:
            return ""

        date = ""
        if len(words[0]) >= 4:
            date += words[0][0:4]
            date += "-"
        else:
            return ""
        
        if len(words[0]) >= 6:
            date += words[0][4:6]
        else:
            date += "01"
        
        date += "-"

        if len(words[0]) >= 8:
            date += words[0][6:8]
        else:
            date += "01"

        return date
    else:
        return ""

def post_document(base_url, token, path, correspondent, tags):
    print("Processing " + path)

    filename = os.path.basename(path)
    fields = []

    with open(path, mode="rb") as f:
        fields.append(urllib3.fields.RequestField.from_tuples('document', (filename, f.read())))

    if correspondent:
        correspondent_id = get_correspondent_by_name(base_url, token, correspondent)
        if correspondent_id == -1: # not found
            correspondent_id = create_correspondent(base_url, token, correspondent)
        
        fields.append(urllib3.fields.RequestField.from_tuples("correspondent",correspondent_id))

    try:
        fields.append(urllib3.fields.RequestField.from_tuples("created",get_date_from_filename(filename)))
    except AttributeError as e:
        print(str(e))

    try:
        fields.append(urllib3.fields.RequestField.from_tuples("title",get_name_from_filename(filename)))
    except AttributeError as e:
        print(str(e))

    if tags:
        for tag in tags.split(","):
            tag_id = get_tag_by_name(base_url, token, tag)
            if tag_id == -1: # not found
                tag_id = create_tag(base_url, token, tag)
            
            fields.append(urllib3.fields.RequestField.from_tuples("tags",tag_id))

    body, header = urllib3.encode_multipart_formdata(fields)

    response = requests.post(base_url + "/api/documents/post_document/", headers={"Authorization":"Token " + token,"content-type":header}, data=body)
    print(response.text)

def post_document_dir(base_url, token, root, files, correspondent, tags):
    for filename in files:
        if not filename.startswith("."):
            post_document(base_url, token, os.path.join(root, filename), correspondent, tags)
        else:
            print("Skipping file " + filename)

parser = argparse.ArgumentParser(
        prog='migrate-paperless-ngx.py',
        description='Migrates your Legacy PDF Documents folder by folder')
parser.add_argument('url', help="paperless-ngx instance without trailing slash, e.g. https://paperless.example.com")
parser.add_argument('token', help="paperless token")
parser.add_argument('path', help="path to your document collection")
parser.add_argument('--correspondent', '-c', help="one correspondent")
parser.add_argument('--tags', '-t', help="one or multiple tags, separated by comma")
parser.add_argument('--no-recursion', help="just take files at the top level", action='store_true')
args = parser.parse_args()

full_path = os.path.abspath(args.path)

if not args.no_recursion:
    for root, subdirs, files in os.walk(full_path):
        post_document_dir(args.url, args.token, root, files, args.correspondent, args.tags)
else:
    files = [f for f in os.listdir(full_path) if os.path.isfile(os.path.join(full_path, f))]
    post_document_dir(args.url, args.token, full_path, files, args.correspondent, args.tags)
