# migrate-paperless-ngx
I needed a tool to migrate my legacy file-based document collection to paperless-ngx. Of course it would have been possible to just dump them in the import directory, but I wanted some more granularity on initial title, tags and correspondent.

The token can be obtained like described in the documentation, I think this is a more clean way than using username and password. See https://docs.paperless-ngx.com/api/#authorization

## File and Directory Structure
By default takes everything below your specified path (including subdirectories), ignoring hidden files (starting with `.`). You can change the recursive behavior with `--no-recursion`. Tries to assume dates from file names (first component of the file name), ignoring all special characters and with a probaly rather fuzzy logic, e.g.
```
2023 -> 01.01.2023
202302 -> 01.02.2023
20230215 -> 15.02.2023
2023.02.15 -> 15.02.2023
2023-02-15 -> 15.02.2023
```

## Command Line Options
These are the command line arguments (you can get them via --help as this will probably be after some further development)
```
usage: migrate-paperless-ngx.py [-h] [--correspondent CORRESPONDENT] [--tags TAGS] [--no-recursion] url token path

Migrates your Legacy PDF Documents folder by folder

positional arguments:
  url                   paperless-ngx instance without trailing slash, e.g. https://paperless.example.com
  token                 paperless token
  path                  path to your document collection

options:
  -h, --help            show this help message and exit
  --correspondent CORRESPONDENT, -c CORRESPONDENT
                        one correspondent
  --tags TAGS, -t TAGS  one or multiple tags, separated by comma
  --no-recursion        just take files at the top level
```
